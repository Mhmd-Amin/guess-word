from tkinter import *


TITLE = 'Guess Word'

class InputWordView():
    __input_letter = None

    def __init__(self, inputWordController):
        self.__inputWordController = inputWordController
        #configire workspace
        self.__ws = Tk()

        #configure workspace
        self.__ws.call('wm', 'iconphoto', self.__ws._w, PhotoImage(file='res/drawable/icon.png'))
        self.__ws.title(TITLE)
        self.__ws.resizable(width=False, height=False)
        self.__ws.config(bg='#9bdde8', width=220, height=100)

        InputWordView.__input_letter = StringVar()

        #label and entry territory
        self.__input_letter.trace('w', InputWordView.check)
        self.__input_label = Label(self.__ws, text='Input Word', justify=CENTER, bg='#9bdde8')
        self.__input_word_entry = Entry(self.__ws, width=12, justify=CENTER, textvariable=self.__input_letter)

        #button territory
        self.__play_btn = Button(self.__ws, text='Play',justify=CENTER, command=lambda:
                self.__inputWordController.play(self.__input_word_entry.get()))

        #widgets position territory
        self.__input_label.place(x=20, y=15)
        self.__input_word_entry.place(x=100, y=15)
        self.__play_btn.place(x=80, y=50)

    
    @property
    def ws(self):
        return self.__ws


    @classmethod
    def check(*args):
        value = InputWordView.__input_letter.get()
        if len(value) > 8: 
            InputWordView.__input_letter.set(value[:8])
        elif len(value) != 0:
            if value[-1].isalpha() == False:
                InputWordView.__input_letter.set(value[:-1])
            elif value[-1].islower() == True:
                InputWordView.__input_letter.set(value.upper())


class PlayGroundView():
    __input_letter = None
    
    def __init__(self, playGroundController, word):
        self.__playGroundController = playGroundController
        self.__widgets = {}
        self.__word_size = len(word)

        #configure workspace
        self.__ws = Tk()
        self.__ws.call('wm', 'iconphoto', self.__ws._w, PhotoImage(file='res/drawable/icon.png'))
        self.__ws.geometry('460x150')
        self.__ws.title(TITLE)
        self.__ws.resizable(width=False, height=False)
        self.__ws.config(width=460, height=150, bg='#9bdde8')
        
        PlayGroundView.__input_letter = StringVar()

        #labels territory
        labels = {}
        up_labels = []
        down_labels = []
        
        start_x_pos = 238-self.__word_size*25
        for i in range(0, self.__word_size):
            up_labels.append(Label(self.__ws, width=3, height=1, justify=CENTER, 
                    font=('Constantia', 12, 'bold'), bg='#9bdde8'))
            up_labels[i].place(x=start_x_pos+i*50, y=12)
            down_labels.append(Label(self.__ws, width=3, height=1, justify=CENTER, font=('Constantia', 12, 'bold'), bg='#9bdde8'))
            down_labels[i].place(x=start_x_pos+i*50, y=47)
            
            labels['up_labels'] = up_labels
            labels['down_labels'] = down_labels

        labels['result_label'] = Label(self.__ws, width=12, height=1, justify=CENTER, bg='#9bdde8')
        labels['result_label'].place(x=161, y=115)
        
        self.__widgets['labels'] = labels

        #lines territory
        canvas = Canvas(self.ws, height=5, width=self.__word_size * 50 - 20, bg='#9bdde8')
        canvas.place(x=240-self.__word_size*25, y=35)
        for i in range(0, self.__word_size):
            canvas.create_rectangle(
                i*50, 0, i*50+30, 5,
                outline="#a2e",
                fill="#fb0")

        #entry territory
        self.__input_letter.trace('w', PlayGroundView.check)
        self.__widgets['entry'] = Entry(self.__ws, width=3, 
            justify=CENTER, textvariable=self.__input_letter)
        self.__widgets['entry'].place(x=240, y=85)

        #button territory
        self.__widgets['button'] = Button(self.__ws,width=6, text='Accept', justify=CENTER, 
            command=lambda: self.__playGroundController.accept())
        self.__widgets['button'].place(x=150, y=82)


    @property
    def widgets(self):
        return self.__widgets


    @property
    def ws(self):
        return self.__ws


    @classmethod
    def check(*args):
        value = PlayGroundView.__input_letter.get()
        if len(value) > 1: 
            PlayGroundView.__input_letter.set(value[0])
        elif value.isalpha() == False:
            PlayGroundView.__input_letter.set('')
        elif value.islower() == True:
            PlayGroundView.__input_letter.set(value.upper())
