from res.views import InputWordView, PlayGroundView
from tkinter import messagebox
from playsound import playsound
import threading

class InputWordController():
    def __init__(self):
        self.__inputWordView = InputWordView(self)
        self.__ws = self.__inputWordView.ws
        self.__ws.mainloop()

    def play(self, word):
        threading.Thread(target=playsound, args=('res/sounds/click.mp3', )).start()
        if self.__isEmpty(word):
            return messagebox.showerror('No Word', 'Doesn`t input word!')
        else:
            remove_widget(self.__ws)
            self.__ws.destroy()
            PlayGroundController(word)

    
    def __isEmpty(self, word):
        if word == '':
            return True
        return False


class PlayGroundController():
    CORRECT_COLOR = '#006C67'
    INCORRECT_COLOR = '#f72119'
    RESULT_COLOR = '#ed6476'

    def __init__(self, word):
        self.__playGroundView = PlayGroundView(self, word)
        self.__widgets = self.__playGroundView.widgets
        self.__ws = self.__playGroundView.ws
        self.__word = word
        self.__word_length = len(self.__word)
        self.__letter_list = list(word)
        self.__mistake = []
        self.__guessed = []
        self.__ws.mainloop()


    def accept(self):
        letter = self.__widgets['entry'].get()
        finded = [i for i, ltr in enumerate(self.__letter_list) if ltr == letter]

        if self.__widgets['button']['text'] == 'Re Match':
            threading.Thread(target=playsound, args=('res/sounds/click.mp3', )).start()
            remove_widget(self.__ws)
            self.__ws.destroy()
            InputWordController()
        elif len(finded) != 0:
            if letter not in self.__guessed:
                threading.Thread(target=playsound, args=('res/sounds/click.mp3', )).start()
                for i in finded:
                    self.__guessed.append(letter)
                    self.__widgets['labels']['up_labels'][i]['text'] = self.__word[i]
                    self.__widgets['labels']['up_labels'][i]['fg'] = PlayGroundController.CORRECT_COLOR
                    self.__widgets['entry'].delete(0, 'end')
                    self.__check_state()
            else:
                self.__widgets['entry'].delete(0, 'end')
        else:
            if letter not in self.__mistake:
                threading.Thread(target=playsound, args=('res/sounds/click.mp3', )).start()
                self.__widgets['labels']['down_labels'][len(self.__mistake)]['text'] = letter
                self.__widgets['labels']['down_labels'][len(self.__mistake)]['fg'] = PlayGroundController.INCORRECT_COLOR
                self.__mistake.append(letter)
                self.__widgets['entry'].delete(0, 'end')
                self.__check_state()
            else:
                self.__widgets['entry'].delete(0, 'end')
                
                
    def __check_state(self):
        if len(self.__guessed) == self.__word_length:
            threading.Thread(target=playsound, args=('res/sounds/victory.mp3', )).start()
            messagebox.showinfo('Victory', 'You win')
            self.__widgets['labels']['result_label']['fg'] = PlayGroundController.RESULT_COLOR
            self.__widgets['labels']['result_label']['font'] = ('Constantia', 12, 'bold')
            self.__widgets['labels']['result_label']['text'] = self.__word
            self.__widgets['button']['text'] = 'Re match'
            self.__widgets['entry']['state'] = 'disable'
        elif len(self.__mistake) == self.__word_length:
            threading.Thread(target=playsound, args=('res/sounds/defeat.mp3', )).start()
            messagebox.showerror('Defeat', 'You lose')
            self.__widgets['labels']['result_label']['fg'] = PlayGroundController.RESULT_COLOR
            self.__widgets['labels']['result_label']['font'] = ('Constantia', 12, 'bold')
            self.__widgets['labels']['result_label']['text'] = self.__word
            self.__widgets['button']['text'] = 'Re match'
            self.__widgets['entry']['state'] = 'disable'


def remove_widget(ws):
    for widgets in ws.winfo_children():
        widgets.destroy()


inputWordController = InputWordController()
